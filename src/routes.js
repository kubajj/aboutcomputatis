import Home from './components/Home.vue'
//import DevelDoc from './components/DevelDoc.vue'
import DevelDoc from './components/Development.vue'
import UserDoc from './components/UserDoc.vue'
import Css from './components/DevelDoc/Css.vue'
import DevelIntro from './components/DevelDoc/DevelIntro.vue'
import DevelOfVue from './components/DevelDoc/DevelOfVue.vue'
import Faq from './components/DevelDoc/Faq.vue'
import Html from './components/DevelDoc/Html.vue'
import Import from './components/DevelDoc/Import.vue'
import Installation from './components/DevelDoc/Installation.vue'
import Publish from './components/DevelDoc/Publish.vue'


export default [
	{ path: '/', component: Home},	
	{ path: '/uzivatel', component: UserDoc},
	{ path: '/vyvojar', component: DevelDoc,
		children: [
		{ path: '/vyvojar', component: DevelIntro },
		{ path: '/vyvojar/css', component: Css },
		{ path: '/vyvojar/vue', component: DevelOfVue },
		{ path: '/vyvojar/faq', component: Faq },
		{ path: '/vyvojar/html', component: Html },
		{ path: '/vyvojar/import', component: Import },
		{ path: '/vyvojar/instalace', component: Installation },
		{ path: '/vyvojar/zverejnenikomponentu', component: Publish },
		]
	},
]